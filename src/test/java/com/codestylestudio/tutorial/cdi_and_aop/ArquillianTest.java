package com.codestylestudio.tutorial.cdi_and_aop;

import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertThat;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ArquillianTest {
	
	private static final String[] DEPENDENCIES = {
			"org.aspectj:aspectjrt:1.8.7"
	};
	
	@Inject
	private Greeter greeter;  
	
	@Deployment
	public static JavaArchive createEnvironement() {
		JavaArchive lib = ShrinkWrap.create(JavaArchive.class, "libs.jar");
	    for (String dependency : DEPENDENCIES) {
	        lib.merge(Maven.resolver().resolve(dependency).withTransitivity().asSingle(JavaArchive.class));
	    }

		JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
				.addPackage("com.codestylestudio.tutorial.cdi_and_aop")
				.addPackage("com.codestylestudio.tutorial.cdi_and_aop.aspects")
				.addAsManifestResource("beans.xml")
				.as(JavaArchive.class);
		
		JavaArchive toBeDeployed = jar.merge(lib);
		
		return toBeDeployed;
	}

	@Test
	public void test() {
		assertThat(greeter.greet("World"), equalToIgnoringCase("hello, world"));
	}

}
